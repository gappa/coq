From Coq Require Import Reals.
From Flocq Require Import Core.
From Gappa Require Import Gappa_tactic.
Open Scope R_scope.

Definition format := generic_format radix2 (FLT_exp (-1074) 53).
Definition rnd := round radix2 (FLT_exp (-1074) 53) ZnearestE.

Goal forall a b : R,
  format a -> format b ->
  (-2 <= a / b <= -1/2)%R ->
  format (a + b).
Proof.
  unfold format; gappa.
Qed.

Goal
  forall a b : R,
  format a -> format b ->
  Rabs (-(rnd (a + b) - (a + b))) <= bpow radix2 (-53) * Rabs (a + b).
Proof.
  unfold format, rnd ; gappa.
Qed.
