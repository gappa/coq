From Coq Require Export Bool ZArith Reals.
From Flocq Require Export Zaux Raux.

Require Export Gappa_real Gappa_definitions Gappa_dyadic.

Lemma Zlt_bool_correct_t :
  forall m n : Z, Zlt_bool m n = true -> (m < n)%Z.
Proof.
intros m n H.
generalize (Zlt_cases m n).
rewrite H.
trivial.
Qed.

Lemma Rdiv_0_r :
  forall x, (x / 0)%R = 0%R.
Proof.
intros x.
unfold Rdiv.
rewrite Rinv_0.
apply Rmult_0_r.
Qed.
